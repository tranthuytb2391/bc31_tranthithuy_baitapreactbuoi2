import React, { Component } from 'react';
import { dataGlasses } from './DataGlasses';
import ItemGlass from './ItemGlass';
import Model from './Model';
import "./Style.css"

export default class Glasses extends Component {
    state = {
        dataGlasses,
        url: undefined,
    }

    handleChangeGlass = (glasses) => {
        let cloneUrl = this.state.url
        let collectUrl = glasses.url
        cloneUrl = collectUrl
        this.setState({
            url: cloneUrl
        })
    }

    renderGlassList = () => {
        return this.state.dataGlasses.map(item => {
            return (
                <ItemGlass
                    glasses={item}
                    key={item.id}
                    handleChangeGlass={this.handleChangeGlass}
                />
            )
        })
    }

    render() {
        return (
            <div className='container vglasses py-3 '>
                <div className="row justify-content-between">
                    <div className="col-6 vglasses__left">
                        <div className="row" id='vglassesList'>
                            {this.renderGlassList()}
                        </div>
                    </div>

                    <div className='col-6 vglasses__right p-0'>
                        <Model
                            virtual={this.state.url}
                        />
                    </div>
                </div>
            </div>
        );
    }
}
