import React, { Component } from 'react';


export default class Model extends Component {
    render() {
        return (
            <div className='vglasses__card'>
                <div className="vglasses__model" id="avatar" style={{ backgroundImage: "url(./Image/model.jpg)" }}>
                    <img src={this.props.virtual} alt="" className='glass-img' />
                </div>
                <div id="glassesInfo" className="vglasses__info">
                </div>
            </div>
        );
    }
}