import React, { Component } from 'react';

export default class Header extends Component {
    render() {
        return (
            <div>
                <div className=" d-flex justify-content-center h-50 d-inline-block bg-secondary">
                    <h2 className="app-title text-white">TRY GLASSES APP ONLINE</h2>
                </div>
            </div>
        );
    }
}

